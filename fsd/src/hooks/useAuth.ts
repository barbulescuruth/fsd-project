import axios from "axios";

const register = (email: string, username: string, password: string) => {

  return axios.post(process.env.API_URL + "/register", {
    email,
    username,
    password,
  });
};

const login = (email: string, password: string) => {

  return axios

    .post(process.env.API_URL + "/login", {
      email,
      password,
    })

    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

const getCurrentUser = () => {

  const currentUser = localStorage.getItem("user");

  if (currentUser != null) {
    return JSON.parse(currentUser);
  }
};


export default {
  register,
  login,
  logout,
  getCurrentUser,
};