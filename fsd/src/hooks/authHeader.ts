export default function authHeader() {

  let user;
  const currentUser = localStorage.getItem("user");

  if (currentUser != null) {
    user = JSON.parse(currentUser);
  }

  if (user && user.accessToken) {
    return { 'x-access-token': user.accessToken };
  } else {
    return {};
  }
}