import React, { useState } from 'react';
import EmailIcon from '@mui/icons-material/Email';
import { PasswordOutlined } from '@mui/icons-material';
import { Grid, Paper, Typography, TextField, Button, Box, Container, CssBaseline, Stack } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";
import loginLogo from './loginLogo.png';


const useStyles = makeStyles({

    root: {
        background: 'linear-gradient(45deg, #0C4AEC 30%, #AE0CEC 90%)',
        border: 3,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        padding: '0 30px',
        height: '100vh',
        width: '100vw'
    },

    paper: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        verticalAlign: "middle",
        boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: "25px",
        height: '80%',
        width: '80%',
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)'
    },

    stack: {
        display: "flex",
        justifyContent: "right",
        alignItems: "center",
        textAlign: "center",
        verticalAlign: "right",
        borderRadius: "25px",
        height: '80%',
        width: '80%',
    }
});

export default function LoginPage() {

    const user = {
        email: "user@yahoo.com",
        password: "user1234"
    }
    const [details, setDetails] = useState({ email: "", password: "" });
    const classes = useStyles();
    const WhiteTextTypography = withStyles({
        root: {
            color: "#FFFFFF"
        }
    })(Typography);

    return (
        <React.Fragment >
            <CssBaseline />
            <Container maxWidth="xl">
                <Box className={classes.root}>
                    <Paper className={classes.paper} >

                        <Grid item xs={6} align-items="center" justify-content="center" display="grid" >
                            <div style={{ position: 'absolute', left: '40%', top: '50%', transform: 'translate(-50%, -50%)' }}>
                                <img src={loginLogo} alt="BigCo Inc. logo" height="90%" width="40%" />
                            </div>
                        </Grid>

                        <Grid item xs={6} alignItems="right">
                            <Stack className={classes.stack} spacing={4}>
                                <div>
                                    <h2>
                                        User Login
                                    </h2>
                                </div>

                                <div>
                                    <TextField
                                        id="filled-required"
                                        label="Email"
                                        placeholder='Enter email'
                                        defaultValue=""
                                        variant="filled"
                                        InputProps={{
                                            endAdornment: (
                                                <EmailIcon />
                                            )

                                        }}
                                        onChange={Text => setDetails({ ...details, email: Text.target.value })} />
                                </div>

                                <div>
                                    <TextField
                                        id="filled-password-input"
                                        label="Password"
                                        placeholder='Enter password'
                                        type="password"
                                        autoComplete="current-password"
                                        variant="filled"
                                        InputProps={{
                                            endAdornment: (
                                                <PasswordOutlined />
                                            )
                                        }}
                                        onChange={Text => setDetails({ ...details, password: Text.target.value })}
                                    />
                                </div>

                                <div>
                                    <Button type='submit' size="large" style={{ maxWidth: '240px', maxHeight: '50px', minWidth: '240px', minHeight: '50px', backgroundColor: '#06C364', color: '#FFFFFF' }} variant="contained" onClick={() => { if (details.email === user.email && details.password === user.password) { alert("✔️ Welcome!"); } }}>
                                        Login
                                    </Button>
                                </div>

                                <div>
                                    <WhiteTextTypography variant="caption" >
                                        <Link href="#" to={'/forgot'}>
                                            Forgot Username/Password?
                                        </Link>
                                    </WhiteTextTypography>
                                </div>
                            </Stack>
                        </Grid>
                    </Paper>
                </Box>
            </Container>
        </React.Fragment>
    );
}