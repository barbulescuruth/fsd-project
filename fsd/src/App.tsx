import './App.scss';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import LoginPage from './login/LoginPage';
import TablePage from './table/TablePage';
import PicturePage from './picture/PicturePage';
import ForgotPage from './forgot/ForgotPage';
import React, { useContext } from 'react';
import DatePage from './datePicker/DatePage';

const loggedIn = React.createContext({ "isLoggedIn": true });

const GuardedRoute = ({ ...props }) => {

  const logged = useContext(loggedIn);
  if (logged.isLoggedIn) {
    return <Route {...props} />;
  }
  return <Redirect to="/" />
}

export default function App() {

  return (
    <Router>
      <div className="page">
        <Switch>
          <Route exact path='/' component={LoginPage}></Route>
          <GuardedRoute exact path='/table'><TablePage /></GuardedRoute>
          <GuardedRoute exact path='/picture' component={PicturePage}></GuardedRoute>
          <GuardedRoute exact path='/forgot' component={ForgotPage}></GuardedRoute>
          <GuardedRoute exact path='/schedule' component={DatePage}></GuardedRoute>
          <Redirect to="/" />
        </Switch>
      </div>
    </Router>

  );
}
