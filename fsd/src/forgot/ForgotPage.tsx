import { Grid, Paper, TextField, Button } from '@mui/material';
import React from 'react';
import EmailIcon from '@mui/icons-material/Email';
import SendIcon from '@mui/icons-material/Send';

export default function ForgotPage() {

    const pageStyle = { padding: 20, height: '70vh', width: 280, margin: "20px auto" }

    return (
        <Grid>
            <Paper elevation={5} style={pageStyle}>
                <div>
                    <h2>
                        Enter your email adress
                    </h2>
                </div>
                <div>
                    <TextField
                        id="filled-required"
                        label="Email"
                        placeholder='Enter email'
                        defaultValue=""
                        variant="filled"
                        InputProps={{
                            endAdornment: (
                                <EmailIcon />
                            )
                        }}
                    />
                </div>
                <div>
                    <Button type='submit' size="medium" style={{ backgroundColor: '#12824C', color: '#FFFFFF' }} variant="contained" onClick={() => { alert("✔️ Message is sent to *@yahoo.com!"); }} endIcon={<SendIcon />} >Send</Button>
                </div>
            </Paper>
        </Grid>

    );
}


