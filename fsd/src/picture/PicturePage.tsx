import { Box, Container, CssBaseline, Button, Stack, styled } from '@mui/material';
import React, { useCallback, useEffect, useState } from 'react';
import companyLogo from './react-logo.png';

const Input = styled('input')({
    display: 'none',
});

export default function PicturePage() {

    const [selectedFile, setSelectedFile] = useState<File>();
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [imageURL, setImageURL] = useState('');

    useEffect(() => {

        if (!selectedFile) return;

        const newImageUrl = URL.createObjectURL(selectedFile);
        setImageURL(newImageUrl);

    }, [selectedFile]);

    function changeHandler(ev: React.ChangeEvent<HTMLInputElement>) {

        if (ev.target.files !== null) {
            setSelectedFile(ev.target.files[0]);
            setIsFilePicked(true);
        }
    }

    const handleSubmission = useCallback(() => {

        if (selectedFile) {
            var img = new Image();
            img.src = imageURL;

            img.onload = function () {
                var height = img.height;
                var width = img.width;

                if (height !== 28 && width !== 28) {
                    alert("Height and Width must be 28x28!");
                } else {
                    localStorage.setItem("imageName", selectedFile.name);
                    console.log('here');
                    const formData = new FormData();
                    formData.append("image", selectedFile);

                    fetch("http://localhost:3001/evaluate", {
                        method: "POST",
                        body: formData,
                    })
                        .then((response) => response.json())
                        .then((result) => {
                            console.log("Success:", result);
                            localStorage.setItem("result", result);
                        })
                        .catch((error) => {
                            console.error("Error:", error);
                        });
                }
            }

        };
    }, [selectedFile, imageURL]);

    useEffect(() => {
        if (isFilePicked) {
            handleSubmission();
            setIsFilePicked(false);
        }

    }, [isFilePicked, handleSubmission]);

    return (
        <React.Fragment >
            <CssBaseline />
            <Container maxWidth="xl">
                <Box sx={{ bgcolor: '#E8EAE9', height: '100vh', width: '100vw', alignItems: 'center' }}>
                    <Stack direction="column" alignItems="center" spacing={10}>
                        <label htmlFor="contained-button-file">
                            <Input accept="image/*" id="contained-button-file" multiple type="file" onChange={ev => changeHandler(ev)} />
                            <Button variant="contained" component="span" onClick={handleSubmission} style={{ backgroundColor: '#4A4A4A', height: '5%', width: '10%', position: 'absolute', textAlign: 'center', left: '80%', bottom: '90%' }}>
                                Upload
                            </Button>
                        </label>
                        <label>
                            <div>
                                {selectedFile &&
                                    <img src={imageURL} className="photo" alt="BigCo Inc. logo" style={{ height: '80%', width: '80%', position: 'absolute', textAlign: 'center', left: '50%', top: '55%', transform: 'translate(-50%, -50%)' }} />
                                }
                                {!selectedFile &&
                                    <img src={companyLogo} className="photo" alt="BigCo Inc. logo" style={{ height: '80%', width: '80%', position: 'absolute', textAlign: 'center', left: '50%', top: '55%', transform: 'translate(-50%, -50%)' }} />
                                }
                            </div>
                        </label>
                    </Stack>
                </Box>
            </Container>
        </React.Fragment>

    )
}