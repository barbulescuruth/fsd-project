import React, { useEffect, useState } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

const columns = [

  { field: 'id', headerName: 'ID', width: 100 },
  { field: 'imageName', headerName: 'Image Name', width: 250 },
  { field: 'size', headerName: 'Size', width: 150 },
  { field: 'recognitionResult', headerName: 'Recognition Result', width: 230 },
  { field: 'downloadLink', headerName: 'Image download link', width: 350, },
];

export default function TablePage() {

  const [rows, setRows] = useState([{ id: '1', imageName: '-', size: '-', recognitionResult: '-', downloadLink: '-' }]);

  useEffect(() => {

    var name = localStorage.getItem("imageName");
    var result = localStorage.getItem("result");
    var nameString: string;
    var resultString: string;

    try {
      nameString = JSON.parse(name || '');
    } catch (ex) {
      nameString = '-';
    }

    try {
      resultString = JSON.parse(result || '');
    } catch (ex) {
      resultString = '-';
    }

    setRows([{ id: '1', imageName: nameString, size: '28 x 28', recognitionResult: resultString, downloadLink: '-' }]);
  }, []);

  return (
    <React.Fragment >
      <CssBaseline />
      <Container maxWidth="xl">
        <Box sx={{ bgcolor: '#E8EAE9', height: '100vh', width: '100vw', alignItems: 'center' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={9}
            rowsPerPageOptions={[9]}
            checkboxSelection
            style={{
              height: '80%', width: '80%', background: '#F6F7F7', position: 'absolute',
              left: '50%',
              top: '50%',
              transform: 'translate(-50%, -50%)'
            }}
          />
        </Box>
      </Container>
    </React.Fragment>
  );
}