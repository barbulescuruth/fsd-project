import { Divider, Box, Container, Grid} from '@mui/material';

import { Button} from 'antd';
import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './DatePopup.scss';

interface DateProps {
    isStartChecked: boolean,
    updateIsStartChecked: (arg: boolean) => void,
    updateIsEndChecked: (arg: boolean) => void,
    updateStartDate: (arg: string) => void,
    updateEndDate: (arg: string) => void,
    updateDatesToSave: (a: string, b: string) => void,
}


export default function DatePopup({isStartChecked, updateIsEndChecked, updateIsStartChecked, updateStartDate, updateEndDate, updateDatesToSave}: DateProps){

    const [startDateToSave, setStartDateToSave] = useState('');
    const [endDateToSave, setEndDateToSave] = useState('');
    const [endDay, setEndDay] = useState('');
    const [startDay, setStartDay] = useState('');
    
    function onCancelClick(){
        updateIsStartChecked(false);
        updateIsEndChecked(false);
        updateStartDate('-');
        updateEndDate('-');
    }

    function onDoneClick(){
        
        if(validation() === true){
            updateDatesToSave(startDateToSave, endDateToSave);
            updateIsStartChecked(false);
            updateIsEndChecked(false);
        }

        updateStartDate('-');
        updateEndDate('-');
        

        
    }

    function validation(){
        console.log(endDay + ' ' + startDay);
        if(endDay === '' || startDay === ''){
            alert('Both endDate and startDate need to be chosen');
            return false;
        } 

        let startDayNum : number = parseInt(startDay);
        let endDayNum : number = parseInt(endDay);

        if(endDayNum < startDayNum){
            alert('End date can not be before start date.');
            return false;
        } 

        return true;
    }

    function onDayClick(value: any){
        let day = value.target.innerText;
        let stringDate: string;
        let dateToSave: string;
        if(day.length === 1){
            stringDate = '0' + day + ' AUG 2021'; 
            dateToSave = '2021-08-0' + day;
        }else{
            stringDate = day + ' AUG 2021'; 
            dateToSave = '2021-08-' + day;
        }
        
        if(isStartChecked){
            setStartDay(day);
            updateStartDate(stringDate);
            setStartDateToSave(dateToSave);
        }else{
            setEndDay(day);
            updateEndDate(stringDate);
            setEndDateToSave(dateToSave);
        }
    }
   

    return (
        <React.Fragment>
            <Container className="calendar">
                <Box className="calendarbox"
                    sx={{
                        width: '100%'
                    }}>

                    <Row className="week" >
                        <Col>
                            <Row>15</Row>
                            <Row>TODAY</Row>
                        </Col>

                        <Divider className="calendardivide" orientation="vertical" flexItem />

                        <Col>
                            <select className="select form-select">
                                
                                <option selected>August</option>

                            </select>
                        </Col>

                        <Divider className="calendardivide" orientation="vertical" flexItem />

                        <Col>
                            <select className="select form-select">
                                <option selected>2021</option>
                            </select>
                        </Col>

                    </Row>
                </Box>

                <Divider className="calendardivide" orientation="horizontal" flexItem />

                <Grid>
                    <Row className="week">
                        <Col><b>SU</b></Col>
                        <Col><b>MO</b></Col>
                        <Col><b>TU</b></Col>
                        <Col><b>WE</b></Col>
                        <Col><b>TH</b></Col>
                        <Col><b>FR</b></Col>
                        <Col><b>SA</b></Col>
                    </Row>

                    <Row className="week">
                        <Col className="other-day">29</Col>
                        <Col className="other-day">30</Col>
                        <Col className="other-day">31</Col>
                        <Col ><Button shape="circle" onClick={onDayClick}>1</Button></Col>
                        <Col ><Button shape="circle" onClick={onDayClick}>2</Button></Col>
                        <Col ><Button shape="circle" onClick={onDayClick}>3</Button></Col>
                        <Col ><Button shape="circle" onClick={onDayClick}>4</Button></Col>
                    </Row>

                    <Row className="week" >
                        <Col> <Button shape="circle" onClick={onDayClick}>5</Button> </Col>
                        <Col><Button shape="circle" onClick={onDayClick}>6</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>7</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>8</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>9</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>10</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>11</Button></Col>
                    </Row>

                    <Row className="week" >
                        <Col><Button shape="circle" onClick={onDayClick}>12</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>13</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>14</Button></Col>
                        <Col className="today"><Button shape="circle" onClick={onDayClick}>15</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>16</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>17</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>18</Button></Col>
                    </Row>

                    <Row className="week" >
                        <Col><Button shape="circle" onClick={onDayClick}>19</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>20</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>21</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>22</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>23</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>24</Button></Col>
                        <Col><Button shape="circle" onClick={onDayClick}>25</Button></Col>
                    </Row>

                    <Row className="week" >
                        <Col>26</Col>
                        <Col className="picker" ><Button shape="circle" onClick={onDayClick}>27</Button></Col>
                        <Col className="picker"><Button shape="circle" onClick={onDayClick}>28</Button></Col>
                        <Col className="picker"><Button shape="circle" onClick={onDayClick}>29</Button></Col>
                        <Col className="picker"><Button shape="circle" onClick={onDayClick}>30</Button></Col>
                        <Col className="picker"><Button shape="circle" onClick={onDayClick}>31</Button></Col>
                        <Col className="other-day">1</Col>
                    </Row>

                    <Row className="week" >
                        <Col className="other-day">2</Col>
                        <Col className="other-day">3</Col>
                        <Col className="other-day">4</Col>
                        <Col className="other-day">5</Col>
                        <Col className="other-day">6</Col>
                        <Col className="other-day">7</Col>
                        <Col className="other-day">8</Col>
                    </Row>
                </Grid>

                <Divider className="calendardivide" orientation="horizontal" flexItem />

                <Box className="calendarbox">
                    <Row className="week">
                        <Col className="buttonCANCEL" onClick={onCancelClick}>CANCEL</Col>
                        <Col className="buttonDONE" onClick={onDoneClick}>DONE</Col>
                    </Row>
                </Box>
                
                
            </Container >
        </React.Fragment >
    )
}