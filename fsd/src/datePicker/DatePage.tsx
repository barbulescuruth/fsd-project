import { Box, Container, CssBaseline, Divider, Grid} from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';
import './DatePage.scss';
import DatePopup from './datePopup/DatePopup';

const columns = [
    { field: 'id', headerName: 'ID', width: 100 },
    { field: 'startDate', headerName: 'Start Date', width: 250 },
    { field: 'endDate', headerName: 'End Date', width: 250 },
];

type Item = {
    id: number, 
    startDate: any,
    endDate: any
}

export default function DatePage() {

    const [startDate, setStartDate] = useState('-');
    const [endDate, setEndDate] = useState('-');
    const [isStartChecked, setIsStartChecked] = useState(false);
    const [isEndChecked, setIsEndChecked] = useState(false);
    const [id, setId] = useState(0);
    const [toDelete, setToDelete] = useState(false);

    const [scheduleData, setScheduleData] = useState<Item[]>([]);

    const updateStartDate = (startDate: string): void => {
        setStartDate(startDate);
    }

    const updateEndDate = (endDate: string): void => {
        setEndDate(endDate);
    }

    const updateIsStartChecked = (isChecked: boolean): void => {
        setIsStartChecked(isChecked);
    }

    const updateIsEndChecked = (isChecked: boolean): void => {
        setIsEndChecked(isChecked);
    }

    function onStartClick() {
        console.log(" start CLICK");
        setIsStartChecked(true);
        setIsEndChecked(false);
    }

    function onEndClick() {
        console.log("end CLICK");
        setIsEndChecked(true);
        setIsStartChecked(false);
    }

    const getSchedules = useCallback(() => {
        let scheduleId = 1;
        let newScheduleData:Item[] = [];
        fetch("https://us-central1-gcp-fsd-project.cloudfunctions.net/getSchedules", {
            method: "GET",
        })
        .then((response) => response.json())
        .then((result) => {
            console.log("Success:", result);
            result.forEach((element: any) => {
                const newElement = {id: scheduleId, startDate: element.startDate, endDate: element.endDate};
                scheduleId++;
                newScheduleData.push(newElement);
            });
            setId(scheduleId);
            setScheduleData(newScheduleData);
        })
        .catch((error) => {
            console.error("Error:", error);
        });
    }, []);

    useEffect(() => {
        getSchedules();
    }, [getSchedules]);

    const deleteRow = useCallback(() => {
        setToDelete(false);
        console.log("delete : " + scheduleData);
        let newScheduleData = Array.from(scheduleData);
        console.log(newScheduleData);
        newScheduleData.pop();
        setScheduleData(newScheduleData);
    }, [scheduleData]);

    const instance = axios.create({
        baseURL: "https://us-central1-gcp-fsd-project.cloudfunctions.net",
        headers: {
            "Content-Type":"application/json"
        }
    });

    async function addSchedule(startDate: string, endDate: string){
        await instance.post('/saveSchedule', {"startDate": startDate, "endDate": endDate})
        .then((response) => console.log("Success: " + response.data))
        .catch((error) => {
            alert(error);
            setToDelete(true);
        });
    }

    useEffect(() => {
        if(toDelete === true){
            deleteRow();
        }
        
    }, [scheduleData, toDelete, deleteRow])

    function addNewRowToTable(startDate: string, endDate: string){
        const newElement = {id: id, startDate: startDate, endDate: endDate};
        setId(id + 1);
        setScheduleData(schedule => [...schedule, newElement]);
    }

    function updateDatesToSave(startDate: string, endDate: string){        
        addNewRowToTable(startDate, endDate);
        addSchedule(startDate, endDate);
    }


    return (
        <React.Fragment >
            <CssBaseline />
            <Grid>
            <Container maxWidth="xl">
                <Box className="box"
                    sx={{
                        display: 'flex',
                        width: 'fit-content',
                        borderRadius: 2,
                        bgcolor: 'background.paper',
                        color: 'text.secondary',
                        '& svg': {
                            m: 1.5,
                        },
                        '& hr': {
                            mx: 3.5,
                        },
                    }}>

                    <div className="buttonLeft" onClick={onStartClick}>
                        <div >
                            Start Date
                        </div>
                        <div className="dateLeftButton">
                            {startDate}
                        </div>
                    </div>

                    <Divider className="divider" orientation="vertical" flexItem />

                    <div className="buttonRight" onClick={onEndClick}>
                        <div >
                            End Date
                        </div>
                        <div className="dateRightButton">
                            {endDate}
                        </div>
                    </div>
                </Box>

                <div className="popup">
                    {(isStartChecked === true ||  isEndChecked === true ) && <DatePopup updateDatesToSave={updateDatesToSave} isStartChecked={isStartChecked} updateIsEndChecked={updateIsEndChecked} updateIsStartChecked={updateIsStartChecked} updateStartDate={updateStartDate} updateEndDate={updateEndDate} />}
                </div>

            </Container>

                <DataGrid
                    rows={scheduleData}
                    columns={columns}
                    pageSize={9}
                    rowsPerPageOptions={[9]}
                    style={{
                    height: '50%', width: '50%', background: '#F6F7F7', position: 'absolute',
                    left: '70%',
                    top: '30%',
                    transform: 'translate(-50%, -50%)'
                    }}
                />
            </Grid>          
         
        </React.Fragment>
    );
}


